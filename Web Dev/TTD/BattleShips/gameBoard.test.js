
let gameBoard = require('./gameBoard');

//const ship = require('./ship')



describe('GameBoard', () => {

    test('should return true', () => {
      let GameBoard  = gameBoard();    
      expect(true).toEqual(true);
    });
    
    test('should have missed attacks set as 0 intially', () => {
        let GameBoard  = gameBoard();  
        expect(GameBoard.missedAttacks.length).toEqual(0);
    });  
    
    expected = [ 0, 1, 2, 'Laura', 'Laura', 5, 6, 7, 8, 9 ];
    test('should return array with "Ship" at index position 3,4', () => {
        let GameBoard  = gameBoard();  
        expect(GameBoard.placeShip(2,3,'Laura')).toEqual(expect.arrayContaining(expected),);
    }); 
    
    test('should have a board array of length 100', () => {
        let GameBoard  = gameBoard();  
        expect(GameBoard.board.length).toEqual(10);
    }); 
    
    test('should have incremented initCount by 1', () => {
        let GameBoard  = gameBoard();  
        GameBoard.placeShip(2,3,'Laura');
        expect(GameBoard.getInitCount()).toEqual(1);
    }); 
    
    test('should have incremented totalLengthOfShips by the length of the ship', () => {
        let GameBoard  = gameBoard();  
        GameBoard.placeShip(2,3,'Laura');
        expect(GameBoard.gettotalLengthOfShips()).toEqual(2);
    }); 
    
    
//    test('implemented ships', () => {
//        const GameBoard  = gameBoard(); 
//        const Ship = ship(5);
//        Ship.hit();
//        GameBoard.placeShip(2, 3, 'la');
//        GameBoard.receiveAttack(3)
//        Ship.getHitCount();
//    }); 
    

    // all ships sunk 
    
    
});