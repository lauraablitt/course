const ship = require('./ship')


describe('Ship', () => {

    test('should return true', () => {
      let Ship = ship(5);    
      expect(true).toEqual(true);
    });
    
    test('should have sunk set as false as default', () => {
        let Ship = ship(5);  
        expect(Ship.sunk).toEqual(false);
    });  
    
    test('should take length as arguments', () => {
        let Ship = ship(5);  
        expect(Ship.length).toEqual(5);
    });
    
    
    test('should take length as a positive number', () => {
        let Ship = ship(5);  
        expect(ship.length).toBeGreaterThan(0);
        
    });
    
    
    test('should return array with its length equal to the ships length', () => {
    let Ship = ship(5);  
    Ship.hit();
    expect(Ship.getHitCount()).toEqual(1);
    });
    
    
    test('should return true as has been hit 5 times', () => {
    let Ship = ship(5);  
    Ship.hit();
    Ship.hit();
    Ship.hit();
    Ship.hit();
    Ship.hit();
    expect(Ship.isSunk()).toEqual(true);
    });

});

