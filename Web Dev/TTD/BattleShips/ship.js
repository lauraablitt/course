//const ship = (length) => {
//    let sunk = false;
//    let segmentsOfShip = [];
//    for(var i = 0; i < length; i++) {
//      segmentsOfShip.push(0);
//    }
//
//   const hit = x => {
//      segmentsOfShip[x] = 'hit';
//      return segmentsOfShip;
//      };
//
//    const isSunk = () => {
//      let count = 0;
//      for(var index in segmentsOfShip) {
//        if (segmentsOfShip[index] === 'hit') {  
//          count++;
//        }
//      }
//      console.log(count);
//      if (count === segmentsOfShip.length) {
//        sunk = true;
//      }
//      return sunk;
//    };
//
//  return { sunk, hit, isSunk, length };
//};

const ship = (length) => {
    let sunk = false;
    let hitCount = 0;

   const hit = () => {
      return hitCount++;
      };

    const isSunk = () => {
      if (hitCount === length) {
        sunk = true;
      }
      return sunk;
    };
    
    const getHitCount = () =>{
        return hitCount;
    };

  return { sunk, hit, isSunk, length, getHitCount }; //might need to add hitCount in here
};

module.exports = ship;