

const gameBoard = () => {
  let initCount = 0;
  let totalLengthOfShips = 0;
  let missedAttacks = [];
  let board = []; 
  for (x = 0; x < 10; x++) {
    board.push(x);
    }
    
  const placeShip = (length, coordinates, shipName) => {
    let shipPositions = []
    for(var i = 0; i < length; i++) {
      shipPositions.push(coordinates+i);
    }
    for (var cell in board) {
      for (var num in shipPositions) {
        if (board[cell] === shipPositions[num]) {
          board[cell] = shipName;
        }
      }
    }
    totalLengthOfShips += length;
    initCount++;
    return board
  };

  const receiveAttack = (coordinates) => {
    if (board[coordinates] !== coordinates) {
          let shipNome = board[coordinates];
          totalLengthOfShips--;
          console.log(shipNome);
          this[shipNome].hit();
    } else {
      missedAttacks.push(coordinates);
    };
  };

  const allShipsSunk = () => {
    if (initCount !== 0 && totalLengthOfShips === 0) {
      return true;
    }
    return false;
  };
    
   const getInitCount = () =>{
    return initCount;
  }
   
   const gettotalLengthOfShips = () =>{
    return totalLengthOfShips;
  }
   
     const getMissedAttacks = () =>{
    return missedAttacks;
  }
  
  return{ missedAttacks, board, placeShip, receiveAttack, allShipsSunk, getInitCount, gettotalLengthOfShips, getMissedAttacks };
};


module.exports = gameBoard;
