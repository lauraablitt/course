from random import shuffle
from random import randint


class Bertrands_Box(object):

    def __init__(self, tries):
        self.tries = tries
        self.boxes = {}
        self.coins = []
        self.count = 0

    def coin_and_boxes_copies(self):
        coins_matching = [["G", "G"], ["S", "S"]]
        coins_opposite = [["G", "S"], ["S", "G"]]
        shuffle(coins_opposite)
        coins = coins_matching + [coins_opposite[0]]
        shuffle(coins)
        self.coins = coins
        return self.coins

    def random_assign_boxes(self):
        for i in range(len(self.coins)):
            self.boxes[i + 1] = self.coins[i]
        return self.boxes

    def random_select_gold(self):
        i = randint(1, 3)
        if self.boxes[i][0] == "G":
            gold = int(i)
            return gold
        else:
            return self.random_select_gold()

    def is_gold(self, gold):
        if self.boxes[gold][1] == "G":
            return True
        else:
            return False

    def play(self):
        self.coin_and_boxes_copies()
        self.random_assign_boxes()
        gold = self.random_select_gold()
        second_coin = self.is_gold(gold)
        if second_coin == True:
            self.count += 1
            return self.count

    def num_of_tries(self):
        for attempt in range(self.tries):
            self.play()
        print(self.count)


if __name__ == '__main__':
    wow = Bertrands_Box(1000)
    wow.num_of_tries()
