import time

class Tamagotchi(object):

    def __init__(self, name):
        self.name = name
        self.hunger = 50
        self.tiredness = 40
        self.happiness = 50
        self.alive = True
        print("Hello, welcome to the world", name)


    def DisplayStats(self):
        print('\nHunger:', self.hunger, '\nTiredness:', self.tiredness, '\nHappiness:', self.happiness)


    def FeedTamagotchi(self):
        self.hunger -= 10
        return self.hunger


    def PlayWithTamagotchi(self):
        self.tiredness += 10
        self.happiness += 10
        return self.tiredness, self.happiness


    def PutTamgotchiToBed(self):
        self.tiredness = 0
        return self.tiredness


    def MakeTamagotchiPoop(self):
        self.hunger += 10
        return self.hunger


    def ChangesOverTime(self):
        self.hunger += 5
        self.tiredness += 5
        self.happiness -= 5
        return self.hunger, self.tiredness, self.happiness


    def Hangry(self):
        if self.hunger >= 70:
            self.happiness = 20
            print('\nI\'m hangry!!!')
        return self.happiness


    def Alive(self):
        if self.hunger >= 100:
            self.alive = False
            print('\nYour Tamagotchi has died of hunger!!!')
            return self.alive
        else:
            self.alive = True
            return self.alive


    def Play(self):
        if not self.Alive():
            return self.alive
        action = input("\nWhat does your Tamagotchi want? Please input one of the following: food, toilet, sleep, play or kill (choose wisely). ")
        if action == "food":
            self.FeedTamagotchi()
        elif action == "toilet":
            self.MakeTamagotchiPoop()
        elif action == "sleep":
            self.PutTamgotchiToBed()
        elif action == "play":
            self.PlayWithTamagotchi()
        elif action == "kill":
            print("\nYOU ARE A MURDERER!!!!!")
            self.alive = False
            return self.alive
        else:
            print("Please enter a valid command")
            self.Play()
        self.DisplayStats()
        time.sleep(1)
        self.Hangry()
        print("\n**** Time has passed ****")
        return True



def play_Tamagotchi(name):
    Laura = Tamagotchi(name)
    while Laura.Play() == True:
        Laura.ChangesOverTime()
        Laura.DisplayStats()
        Laura.Play()


if __name__ == '__main__':
    wow = play_Tamagotchi("Vor")