import unittest
from virtualpet import Tamagotchi

class testfunc(unittest.TestCase):

    def setUp(self):
        print("setting up")
        self.virtualpet = Tamagotchi("Laura")

    def tearDown(setUp):
        print()

    def test_FeedTamagotchi(self):

        self.virtualpet.hunger = 50
        self.virtualpet.FeedTamagotchi()
        self.assertEqual(40, self.virtualpet.hunger)

    def test_PlayWithTamagotchi(self):

        self.virtualpet.tiredness = 50
        self.virtualpet.happiness = 50
        self.virtualpet.PlayWithTamagotchi()
        self.assertEqual(60, self.virtualpet.tiredness, self.virtualpet.happiness)


    def test_PutTamgotchiToBed(self):

        self.virtualpet.tiredness = 100
        self.virtualpet.PutTamgotchiToBed()
        self.assertEqual(0, self.virtualpet.tiredness)

    def test_MakeTamagotchiPoop(self):

        self.virtualpet.hunger = 50
        self.virtualpet.MakeTamagotchiPoop()
        self.assertEqual(60, self.virtualpet.hunger)


    def test_ChangesOverTime(self):

        self.virtualpet.hunger = 50
        self.virtualpet.tiredness = 50
        self.virtualpet.happiness = 50
        self.virtualpet.ChangesOverTime()
        self.assertEqual((55, 55, 45), (self.virtualpet.hunger, self.virtualpet.tiredness, self.virtualpet.happiness))


    def test_Hangry(self):

        self.virtualpet.hunger = 70
        self.virtualpet.Hangry()
        self.assertEqual(20, self.virtualpet.happiness)



if __name__ == '__main__':
    unittest.main()
