
def greet(names):
    lower_names = [name for name in names if name.islower()]
    UPPER_names = [name for name in names if name.isupper()]
    print('hello, ', end="")
    for name in lower_names[:-1]:
        print(name.title(), end=", ")
    print('and {}.'.format(lower_names[-1]))

    print('AND HELLO, ', end="")
    for name in UPPER_names[:-1]:
        print(name, end=", ")
    print('AND {}.'.format(UPPER_names[-1]))

names = ['LAURA', 'Ben', 'Robert', 'john', 'steve', 'GEORGE']

greet(names)
