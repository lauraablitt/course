
palindrome_list = []

for num1 in range(999):
    for num2 in range(999):
        palindrome = num1 * num2
        palindrome = str(palindrome)
        first_half_palindrome = palindrome[:3]
        second_half_palindrome = palindrome[3:]
        second_half_palindrome = second_half_palindrome[::-1]
        if first_half_palindrome == second_half_palindrome:
            palindrome_list.append(palindrome)

print('the largest palindrome is', max(palindrome_list))
