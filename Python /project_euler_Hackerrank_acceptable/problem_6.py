


def Sum_square_difference(n):
    n = int(n)
    if n<=10000 and n>= 1:
        sum = 0
        sum_squared = 0
        list = []


        for i in range(1,(n+1)):
            list.append(i)

        for num in list:
            sum += num

        sum_squared = (n * (n + 1) * (2 * n + 1) / 6)


        square_of_sum = sum**2
        difference = square_of_sum - sum_squared
        return difference

print(Sum_square_difference(9999))