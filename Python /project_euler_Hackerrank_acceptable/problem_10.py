
def Sieve(n):
    list_of_num = [True] * (n + 1)
    dict_of_sums = {}
    sum = 0
    for j in range(2, round(n ** 0.5)):
        if list_of_num[j] is True:
            i = j*2
            while i <= n:
                list_of_num[i] = False
                i += j
    for i in range(2, n + 1):
        if list_of_num[i] is True:
            sum += i
        dict_of_sums[i] = sum

    return dict_of_sums

t = int(input().strip())
list = []
for a0 in range(t):
    n = int(input().strip())
    list.append(n)
maxn = max(list)
if maxn > 100:
    dict = Sieve(maxn)
else:
    dict = Sieve(100)
for k in list:
    print(dict.get(k))