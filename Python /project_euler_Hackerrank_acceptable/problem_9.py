def triplet(n):

    lst = []
    A = round(n / 3)
    for a in range(1, A):
        b = (n**2 - 2*n*a) / (2 * (n-a))
        if b%1 == 0:
            c = n - b - a
            if a < b < c:
                if a ** 2 + b ** 2 == c ** 2:
                    lst.append(a*b*c)
    if len(lst) == 0:
        return -1
    else:
        return round(max(lst))

t = int(input().strip())
for a0 in range(t):
    n = int(input().strip())
    print(triplet(n))