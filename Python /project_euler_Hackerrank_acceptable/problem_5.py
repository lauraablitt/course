from functools import reduce
import math




def Smallest_multiple(n):

    if n == 0 or n==1:
        answer = 1
        return answer
    primes = []
    required_primes_squared = []

    for possiblePrime in range(2, 1000):

       prime = True
       for num in range(2, int(possiblePrime ** 0.5) + 1):
           if possiblePrime % num == 0:
               prime = False
               break

       if prime:
           primes.append(possiblePrime)


    for num in primes:
        if num <= n:

            k = math.log(n, num)
            k = int(k)

            required_primes_squared.append(num**k)


    answer = reduce(lambda x, y: x*y, required_primes_squared)

    return answer


print(Smallest_multiple(n))


