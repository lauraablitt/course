primes = []
for possiblePrime in range(2, 600851475143):

   prime = True
   for num in range(2, int(possiblePrime ** 0.5) + 1):
       if possiblePrime % num == 0:
           prime = False
           break

   if prime:
       primes.append(possiblePrime)