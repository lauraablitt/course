def product_series(num, k):

    max = 0
    string = str(num)

    for i in range(len(string)-k+1):
        lst = []
        for j in range(k):
            lst.append(int(string[j+i]))
        bam = 1
        for x in lst:
            bam *= x
        if bam > max:
            max = bam
    return max

t = int(input().strip())
for a0 in range(t):
    n,k = input().strip().split(' ')
    n,k = [int(n),int(k)]
    num = input().strip()
    print(product_series(num, k))