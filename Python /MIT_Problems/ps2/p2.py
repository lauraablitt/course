# Problem Set 2, hangman.py
# Name: Laura Ablitt
# Collaborators:
# Time spent: hourssssssssssssss

# Hangman Game
# -----------------------------------
# Helper code
# You don't need to understand this helper code,
# but you will have to know how to use the functions
# (so be sure to read the docstrings!)
import random
import string
WORDLIST_FILENAME = "words.txt"


def load_words():
    """
    Returns a list of valid words. Words are strings of lowercase letters.
    
    Depending on the size of the word list, this function may
    take a while to finish.
    """
    print("Loading word list from file...")
    # inFile: file
    inFile = open(WORDLIST_FILENAME, 'r')
    # line: string
    line = inFile.readline()
    # wordlist: list of strings
    wordlist = line.split()
    print("  ", len(wordlist), "words loaded.")
    return wordlist



def choose_word(wordlist):
    """
    wordlist (list): list of words (strings)
    
    Returns a word from wordlist at random
    """
    return random.choice(wordlist)
# end of helper code
# -----------------------------------

# Load the list of words into the variable wordlist
# so that it can be accessed from anywhere in the program
wordlist = load_words()


def is_word_guessed(secret_word, letters_guessed):
    '''
    secret_word: string, the word the user is guessing; assumes all letters are
      lowercase
    letters_guessed: list (of letters), which letters have been guessed so far;
      assumes that all letters are lowercase
    returns: boolean, True if all the letters of secret_word are in letters_guessed;
      False otherwise
    '''

    for char in secret_word:
        if char in letters_guessed:
            return True
        return False

def is_letter_guessed(secret_word, letters_guessed, guess):
    '''
    secret_word: string, the word the user is guessing; assumes all letters are
      lowercase
    letters_guessed: list (of letters), which letters have been guessed so far;
      assumes that all letters are lowercase
    returns: boolean, True if the letter of the letters_guessed is in the secret word;
      False otherwise
    '''

    if letters_guessed[guess] in secret_word:
        return True
    else:
        return False

def get_guessed_word(secret_word, letters_guessed):
    '''
    secret_word: string, the word the user is guessing
    letters_guessed: list (of letters), which letters have been guessed so far
    returns: string, comprised of letters, underscores (_), and spaces that represents
      which letters in secret_word have been guessed so far.
    '''
    num_correct_letters = 0
    guessed_word = " "
    for char in secret_word:
        if char in letters_guessed:
            guessed_word += char + " "
            num_correct_letters += 1
        else:
            guessed_word += "_ "
    return (guessed_word, num_correct_letters)



def get_available_letters(letters_guessed):
    '''
    letters_guessed: list (of letters), which letters have been guessed so far
    returns: string (of letters), comprised of letters that represents which letters have not
      yet been guessed.
    '''
    alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
    for char in letters_guessed:
        if char in alphabet:
            alphabet.remove(char)
    return alphabet
    
    

def hangman(secret_word):
    '''
    secret_word: string, the secret word to guess.
    
    Starts up an interactive game of Hangman.
    
    * At the start of the game, let the user know how many
      letters the secret_word contains and how many guesses s/he starts with.
      
    * The user should start with 6 guesses

    * Before each round, you should display to the user how many guesses
      s/he has left and the letters that the user has not yet guessed.
    
    * Ask the user to supply one guess per round. Remember to make
      sure that the user puts in a letter!
    
    * The user should receive feedback immediately after each guess 
      about whether their guess appears in the computer's word.

    * After each guess, you should display to the user the 
      partially guessed word so far.
    
    Follows the other limitations detailed in the problem write-up.
    '''
    print('Welcome to the game Hangman!')
    print("I am thinking of a word that is ", len(secret_word), " letters long.")

    guesses = range(15)
    letters_guessed = []
    for guess in guesses:
        print("you have", (len(guesses) - guess), "guesses left!")
        print("available letters", get_available_letters(letters_guessed))
        letter = input(str("please enter a letter:- "))
        letters_guessed.append(letter)
        current_word = get_guessed_word(secret_word, letters_guessed)
        print(current_word[0])
        if current_word[1] >= len(secret_word):
            if is_word_guessed(secret_word, letters_guessed) is True:
                print("Congratulations you clever soul")
                break
        if guess == 14:
            print("you didn't get it loser, the secret word was", secret_word)


secret_word = choose_word(wordlist)
hangman(secret_word)
