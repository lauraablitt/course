

def sal(ps, annual_salary):
    
    total_cost = 1000
    portion_down_payment = (0.25*total_cost)
    current_savings = 0
    monthly_salary=annual_salary/12
    portion_saved_per_month = (ps/100)*monthly_salary
    month_no = 1
    current_savings = portion_saved_per_month
    Semiannual_salary_raise = 7
    while current_savings < portion_down_payment:
        current_savings = current_savings + (current_savings*(0.04/12))
        month_no = month_no + 1
        current_savings = current_savings + portion_saved_per_month
        if month_no%6 == 0:
            annual_salary = annual_salary + (annual_salary*Semiannual_salary_raise/100)
            monthly_salary=annual_salary/12
            portion_saved_per_month = monthly_salary*(ps/100)
    return month_no

epsilon = 1
annual_salary = 300
high = 100
low = 0
guess = ((1)/2)
test = sal(guess, annual_salary) 
while test != 36:
    if test > 36:
        low = guess
    elif test < 36:
        high = guess
    guess = ((low+high)/2)
    test = sal(guess, annual_salary)         