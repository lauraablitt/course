# 6.0001 Problem Set 3
#
# The 6.0001 Word Game
# Created by: Kevin Luu <luuk> and Jenna Wiens <jwiens>
#
# Name          : <your name>
# Collaborators : <your collaborators>
# Time spent    : <total time>

import math
import random
import string

VOWELS = 'aeiou'
CONSONANTS = 'bcdfghjklmnpqrstvwxyz'
HAND_SIZE = 7

SCRABBLE_LETTER_VALUES = {
    'a': 1, 'b': 3, 'c': 3, 'd': 2, 'e': 1, 'f': 4, 'g': 2, 'h': 4, 'i': 1, 'j': 8, 'k': 5, 'l': 1, 'm': 3, 'n': 1, 'o': 1, 'p': 3, 'q': 10, 'r': 1, 's': 1, 't': 1, 'u': 1, 'v': 4, 'w': 4, 'x': 8, 'y': 4, 'z': 10, '*':0
}

# -----------------------------------
# Helper code
# (you don't need to understand this helper code)

WORDLIST_FILENAME = "words.txt"

def load_words():
    """
    Returns a list of valid words. Words are strings of lowercase letters.
    
    Depending on the size of the word list, this function may
    take a while to finish.
    """
    
    print("Loading word list from file...")
    # inFile: file
    inFile = open(WORDLIST_FILENAME, 'r')
    # wordlist: list of strings
    word_list = []
    for line in inFile:
        word_list.append(line.strip().lower())
    print("  ", len(word_list), "words loaded.")
    return word_list

def get_frequency_dict(sequence):
    """
    Returns a dictionary where the keys are elements of the sequence
    and the values are integer counts, for the number of times that
    an element is repeated in the sequence.

    sequence: string or list
    return: dictionary
    """
    
    # freqs: dictionary (element_type -> int)
    freq = {}
    for x in sequence:
        freq[x] = freq.get(x,0) + 1
    return freq
	

# (end of helper code)
# -----------------------------------

#
# Problem #1: Scoring a word
#
def get_word_score(word, n):

    word = word.lower()
    # first part
    score = 0
    for letter in word:
        value = SCRABBLE_LETTER_VALUES.get(letter, int(0))
        score = value + score
    # second part
    Second_component = 7 * len(word) - 3 * (n - len(word))
    if Second_component > 1:
        score = Second_component * score
    else:
        score
    return score


def display_hand(hand):
    """
    Displays the letters currently in the hand.

    For example:
       display_hand({'a':1, 'x':2, 'l':3, 'e':1})
    Should print out something like:
       a x x l l l e
    The order of the letters is unimportant.

    hand: dictionary (string -> int)
    """
    
    for letter in hand.keys():
        for j in range(hand[letter]):
             print(letter, end=' ')      # print all on the same line
    print()                              # print an empty line

#
# You will need to modify this for Problem #4.
#
def deal_hand(n):
    """
    Returns a random hand containing n lowercase letters.
    ceil(n/3) letters in the hand should be VOWELS (note,
    ceil(n/3) means the smallest integer not less than n/3).

    Hands are represented as dictionaries. The keys are
    letters and the values are the number of times the
    particular letter is repeated in that hand.

    n: int >= 0
    returns: dictionary (string -> int)
    """
    
    hand={}
    num_vowels = int(math.ceil(n / 3))

    for i in range(num_vowels):
        x = random.choice(VOWELS)
        hand[x] = hand.get(x, 0) + 1
    
    for i in range(num_vowels, n-1):
        x = random.choice(CONSONANTS)
        hand[x] = hand.get(x, 0) + 1

    hand['*'] = int(1)

    return hand

#
# Problem #2: Update a hand by removing letters
#
def update_hand(hand, word):

    handCopy = hand.copy()
    word = word.lower()
    for letter in word:
        value = handCopy.get(letter)
        value -= 1
        handCopy[letter] = value
    return handCopy
#
# Problem #3: Test word validity
#
def is_valid_word(word, hand, word_list):

    word = word.lower()

    def is_word_in_word_list():
        if word in word_list:
            return True
        if '*' in word:
            for vowel in VOWELS:
                if word.replace('*', vowel) in word_list:
                    return True
            for consonent in CONSONANTS:
                if word.replace('*', consonent) in word_list:
                    return True
            return False

    handCopy = hand.copy()
    if is_word_in_word_list() is True:
        neg = 0
        for letter in word:
            if letter in handCopy:
                value = handCopy.get(letter)
                value = value - 1
                handCopy[letter] = value
            else:
                return False
            if value < 0:
                neg += 1
            if neg > 0:
                return False
        return True



# Problem #5: Playing a hand

def calculate_handlen(hand):

    return int(len(hand))

def play_hand(hand, word_list):
    total_score = 0
    while calculate_handlen(hand) > 0:
        print("Current Hand:", end=" ")
        display_hand(hand)
        word = input('Enter word, or "!!" to indicate that you are finished: ')
        if word == '!!':
            break
        else:
            if is_valid_word(word, hand, word_list):
                word_score = get_word_score(word, calculate_handlen(hand))
                total_score += word_score
                print('"' + word + '"', "earned", word_score, "points. Total:", total_score, "points")

            else:
                print("That is not a valid word. Please choose another word.")
            hand = update_hand(hand, word)
    if calculate_handlen(hand) == 0:
        print("Ran out of letters.", end=' ')
    print("Total score for this hand:", total_score, "points")
    print("-" * 10)


def substitute_hand(hand, letter):


    if letter not in hand:
        return hand

    new_letter_found = False

    while not new_letter_found:
        new_letter = random.choice(VOWELS + CONSONANTS)
        if new_letter not in hand:
            new_letter_found = True
            new_hand = dict()
            for key, value in hand.items():
                if key == letter:
                    new_hand[new_letter] = value
                else:
                    new_hand[key] = value

    return new_hand


def play_game(word_list):


    total_hands = int(input('Enter total number of hands: '))
    final_score = 0
    letter_substituted = False
    hand_replayed = False

    for n in range(total_hands):
        hand = deal_hand(HAND_SIZE)
        if not letter_substituted:
            print("Current Hand:", end=" ")
            display_hand(hand)
            substitute = input('Would you like to substitute a letter? ')
            if substitute.lower() == 'yes':
                letter_substituted = True
                letter = input('Which letter would you like to replace: ')
                hand = substitute_hand(hand, letter)
        hand_score = play_hand(hand, word_list)
        if not hand_replayed:
            replay = input('Would you like to replay the hand? ')
            if replay.lower() == 'yes':
                hand_replayed = True
                new_hand_score = play_hand(hand, word_list)
                if new_hand_score > hand_score:
                    hand_score = new_hand_score
        final_score += hand_score

    print("Total score over all hands:", final_score)
    


#
# Build data structures used for entire session and play game
# Do not remove the "if __name__ == '__main__':" line - this code is executed
# when the program is run directly, instead of through an import statement
#
if __name__ == '__main__':
    word_list = load_words()
    play_game(word_list)

