
def permutate_and_build(word):

    # return list type as this is the same as the results type
    if len(word) <= 1:
        return [word]

    # call until remaining word is only 1 character long
    remaining_word = word[1:]
    sequences = permutate_and_build(remaining_word)

    # make empty list for results
    results = []

    # get the first letter to iterate through the remaining word
    first_letter = word[0]

    for sequence in sequences:
        for i in range(len(sequence)+1):
            results.append(sequence[:i] + first_letter + sequence[i:])

    return results

print(permutate_and_build('bust'))