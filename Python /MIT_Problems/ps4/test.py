import string


### HELPER CODE ###
def load_words(file_name):
    '''
    file_name (string): the name of the file containing
    the list of words to load

    Returns: a list of valid words. Words are strings of lowercase letters.

    Depending on the size of the word list, this function may
    take a while to finish.
    '''
    print("Loading word list from file...")
    # inFile: file
    inFile = open(file_name, 'r')
    # wordlist: list of strings
    wordlist = []
    for line in inFile:
        wordlist.extend([word.lower() for word in line.split(' ')])
    print("  ", len(wordlist), "words loaded.")
    return wordlist


def is_word(word_list, word):
    '''
    Determines if word is a valid word, ignoring
    capitalization and punctuation

    word_list (list): list of words in the dictionary.
    word (string): a possible word.

    Returns: True if word is in word_list, False otherwise

    Example:
    >>> is_word(word_list, 'bat') returns
    True
    >>> is_word(word_list, 'asdf') returns
    False
    '''
    word = word.lower()
    word = word.strip(" !@#$%^&*()-_+={}[]|\:;'<>?,./\"")
    return word in word_list


def get_story_string():
    """
    Returns: a story in encrypted text.
    """
    f = open("story.txt", "r")
    story = str(f.read())
    f.close()
    return story


### END HELPER CODE ###

WORDLIST_FILENAME = 'words.txt'


def build_shift_dict(shift):
    '''
    Creates a dictionary that can be used to apply a cipher to a letter.
    The dictionary maps every uppercase and lowercase letter to a
    character shifted down the alphabet by the input shift. The dictionary
    should have 52 keys of all the uppercase letters and all the lowercase
    letters only.

    shift (integer): the amount by which to shift every letter of the
    alphabet. 0 <= shift < 26

    Returns: a dictionary mapping a letter (string) to
             another letter (string).
    '''

    shift_dict = {}
    alphabet_length = len(string.ascii_lowercase)

    for char in string.ascii_lowercase:
        if ord(char) > (122 - shift):
            char_shift = ord(char) - alphabet_length + shift
            shift_dict[char] = chr(char_shift)
        elif ord(char) < (97 - shift):
            char_shift = ord(char) + alphabet_length + shift
            shift_dict[char] = chr(char_shift)
        else:
            char_shift = ord(char) + shift
            shift_dict[char] = chr(char_shift)

    for char in string.ascii_uppercase:
        if ord(char) > (90 - shift):
            char_shift = ord(char) - alphabet_length + shift
            shift_dict[char] = chr(char_shift)
        elif ord(char) < (65 - shift):
            char_shift = ord(char) + alphabet_length + shift
            shift_dict[char] = chr(char_shift)
        else:
            char_shift = ord(char) + shift
            shift_dict[char] = chr(char_shift)

    return shift_dict


# Returns: a tuple of the best shift value used to decrypt the message
#         and the decrypted message text using that shift value


# need to split into 2 words if long
def decrypt_message(string):


    count_match = []

    for guess_shift in range(26):
        test_shift = apply_shift(guess_shift)
        decipher_word_list = [test_word.strip(" !@#$%^&*()-_+={}[]|\:;'<>?,./\"") for test_word in test_shift.split(" ")]
        count_match.append(sum([is_word(wordlist, test_word) for test_word in decipher_word_list]))

    best_shift = count_match.index(max(count_match))
    decrypted_word = apply_shift(best_shift)


    return (best_shift, decrypted_word)





string = 'hello world'
print(decrypt_message(string))