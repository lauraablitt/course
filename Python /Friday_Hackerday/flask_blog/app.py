from flask import Flask, render_template, request, redirect, url_for
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime

app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////Users/ablittl/Documents/THG_Software/Projects/Week_5/flask_blog/blog.db'


db = SQLAlchemy(app)

class Blogpost(db.Model):
     id = db.Column(db.Integer, primary_key=True)
     title = db.Column(db.String(50))
     subtitle = db.Column(db.String(50))
     author = db.Column(db.String(20))
     date_posted = db.Column(db.DateTime)
     content = db.Column(db.Text)

class CommentsDataBase(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    author = db.Column(db.String(20))
    date_posted = db.Column(db.DateTime)
    comments = db.Column(db.Text)
    rel_comment_id = db.Column(db.Integer, db.ForeignKey('blogpost.id'))

@app.route('/')
def index():
    posts = Blogpost.query.order_by(Blogpost.date_posted.desc()).all()
    print(posts)

    return render_template('index.html', posts=posts)

@app.route('/about')
def about():
     return render_template('about.html')

@app.route('/post/<int:post_id>')
def post(post_id):
    post = Blogpost.query.filter_by(id=post_id).one()
    comment = CommentsDataBase.query.filter_by(rel_comment_id=post_id)
    return render_template('post.html', post=post, comment=comment,post_id=post_id)

@app.route('/add')
def add():
    return render_template('add.html')

@app.route('/comment', methods=['POST'])
def comment():
    author = request.form['author']
    comments = request.form['comments']# request Comments on the pages
    rel_comment_id = request.form['post_id']

    comment = CommentsDataBase(author=author, comments=comments, date_posted=datetime.now(), rel_comment_id=rel_comment_id)

    db.session.add(comment)
    db.session.commit()

    return  redirect(url_for('index'))

@app.route('/addpost', methods=['POST'])
def addpost():
    title = request.form['title']
    subtitle = request.form['subtitle']
    author = request.form['author']
    content = request.form['content']

    post = Blogpost(title=title, subtitle=subtitle, author=author, content=content, date_posted=datetime.now())

    db.session.add(post)
    db.session.commit()

    return redirect(url_for('index'))

if __name__ == '__main__':
    app.run(debug=True)