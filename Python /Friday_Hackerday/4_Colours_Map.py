A = 'A'
B = 'B'
C = 'C'
D = 'D'
E = 'E'
F = 'F'
G = 'G'
H = 'H'
I = 'I'
J = 'J'
K = 'K'
L = 'L'
M = 'M'
N = 'N'
O = 'O'

Country2 = {A: (B, F, G), B: (A, C, G, H, I), C: (B, I, D), D: (C, I, E, J, N, M), E: (D, J), F: (A, G, K),
            G: (A, F, B, H, K, L), H: (B, G, I, L, M), I: (B, C, D, H, M), J: (D, E, N, O), K: (F, G, L),
            L: (G, K, H, M), M: (H, I, L, D, N), N: (D, J, M, O), O: (J, N)}

colours = [1, 2, 3, 4]

d = {}
for key, value in Country2.items():
    d[key] = len([item for item in value if item])
ddd = [(v, k) for k, v in d.items()]
ddd.sort()
ddd.reverse()
ddd = [(k, v) for v, k in ddd]
neighbours_dict = {row[0]: row[1] for row in ddd}

vertices = []
for key in neighbours_dict:
    vertices.append(key)

# vertices1 = [G,D,M,I,H,B,N,L,J,K,F,C,A,O,E]
print(vertices)


# print(vertices1)

def colouring(dictionary):
    colordict = {1: [], 2: [], 3: [], 4: []}
    while len(vertices) != 0:
        for k in range(1, 5):
            temporary_list = []
            temporary_list.append(vertices[0])
            colordict[int(k)].append(temporary_list[0])
            for j in range(1, len(vertices)):
                try:
                    if all(vertices[j] not in dictionary[item] for item in temporary_list):
                        colordict[k].append(vertices[j])
                        temporary_list.append(vertices[j])
                except IndexError:
                    pass
            for element in temporary_list:
                vertices.remove(element)
    print(colordict)

    return colordict


colouring(Country2)