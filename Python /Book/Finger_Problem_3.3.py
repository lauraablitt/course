# What would you have to change to make the below code allow negative and 
# positive numbmers.

x = 24
epsilon = 0.01
numGuesses = 0
low = min(0, x)
high = max(1, x)
anss = (high + low)/2
while abs(anss**2 - x) >= epsilon:
    print('low =', low, 'high =', high, 'ans =', anss)
    numGuesses += 1
    if anss**2 < x:
        low = anss
    else:
        high = anss
    anss = (high + low)/2
print('numGuesses =', numGuesses)
print(anss, 'is close to square root of', x)

