# Write a Python class which has two methods get_String and print_String. get_String accept a string from the user and
# print_String print the string in upper case.


# define the class (type)
class String:
    # define the variables which will be inputted into the class
    def __init__(self):
        self.str1 = ""

    # actually get the string from the class
    def getstring(self):
        self.str1 = input("please type your string here ")
    # print the string in upper case.
    def print_string(self):
        print(self.str1.upper())

# define the string (str1). the class is defining the type, so need to define variable outside.
str1 = String()
# get the string (str1)
str1.getstring()
# print the string (str1)
str1.print_string()