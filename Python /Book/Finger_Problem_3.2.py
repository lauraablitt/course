# range(start, stop, step)
# Let s be the string that contains a sequence of decimal numbers separated by 
# commas, e.g., s = '1.23,2.4,3.123'. Write a program that prints the sum of 
# the numbers in s

s = [1.23, 2.4, 3.123] 
sum = 0

for i in range(len(s)):
    sum = s[i] + sum
    
print("The sum of the numbers is", sum)
    
    

