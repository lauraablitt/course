# Write a function that accepts two strings as arguments and returns True if 
# either string occurs anywhere in the other, and False otherwise. 

def ifin(s1,s2):
    if s1 in s2:
        return True
    elif s2 in s1:
        return True
    else:
        return False
    
