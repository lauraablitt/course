# Write a Python class named Rectangle constructed by a length and width and a method which will compute the area of a rectangle.


# define the class
class rectangle:
    # define the initial variables
    def __init__(self, width, length):
        self.width = width
        self.length = length

    # define what the function area does
    def area(self):
        area = self.width * self.length
        return area

# it can be called through this
newRectangle = rectangle(12, 10)
print(newRectangle.area())
# it can be called through this - they are both the same
print(rectangle(12, 10).area())
