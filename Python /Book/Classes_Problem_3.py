# Write a Python class named Circle constructed by a radius and two methods which will compute the area and the
# perimeter of a circle

class Circle:
    # define the initial variables
    def __init__(self, radius):
        self.radius = radius

    def area(self):
        area = 3.1452*(self.radius ** 2)
        return area

    def perimeter(self):
        return 2 * self.radius * 3.14

print(Circle(10).area())
print(Circle(10).perimeter())