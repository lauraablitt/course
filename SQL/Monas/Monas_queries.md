1. 
<i>Find the IDs of all students who were taught by an instructor named Einstein; make sure there are no duplicates in the result.</i>
```SQL
SELECT DISTINCT student.id AS 'Students taught by Eistein'
FROM instructor, student, advisor
WHERE advisor.s_ID = student.ID AND advisor.i_ID = instructor.ID and instructor.name = 'Einstein';
```
<i>Find all instructors earning the highest salary (there may be more than one with the same salary).</i>
```SQL
SELECT instructor.name, instructor.salary
FROM instructor
Where instructor.salary = (select max(salary) from instructor);
```
<i>Find the enrolment of each section that was offered in Autumn 2009.</i>
```SQL
SELECT takes.id, section.course_id
FROM takes, section
WHERE section.course_id = takes.course_id AND section.semester = 'Fall'
ORDER BY takes.id;
```
<i>Find the maximum enrolment, across all sections, in Autumn 2009.</i>
```SQL
SELECT max(counts_per_sec)
FROM (Select count(takes.sec_id) as select_query_name from takes group by takes.sec_id ) as temp_table;
```

2. 

<i>Find the total grade-points earned by the student with ID 12345, across all courses taken by the student.</i>
```SQL
Select sum(grade_credit.credit * course.credits)/sum(course.credits)
from (takes natural join course) natural join grade_credit
where ID = '12345';
```

<i>Find the grade-point average (GPA) for the above student, that is, the total grade-points divided by the total credits for the associated courses.</i>
```SQL
Select sum(grade_credit.credit * course.credits)
from (takes natural join course) natural join grade_credit
where ID = '12345';
```

